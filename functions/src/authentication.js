exports.eliminarUsuario = (functions, admin) => {
    return functions.auth.user().onDelete(event => {
        const user = event;
        console.log('Usuario eliminado: ', user);

        if(user.email.indexOf('@cloudtestlabaccounts.com') > -1){
            console.log('Eliminar usuario googlecloud');
            admin.firestore().collection('grupos')
                .where('uidUserPropietario', '==', user.uid)
                .get()
                .then(querySnapshot => {
                    querySnapshot.forEach(documentSnapshot => {
                        console.log(`Found document at ${documentSnapshot.ref.path}`);
                        admin.firestore()
                            .doc(documentSnapshot.ref.path)
                            .delete()
                            .then(() => {
                                console.log(`Document "${documentSnapshot.ref.path}" successfully deleted.`);
                                return true;
                            })
                            .catch(exception => {
                                console.log('Error!: ' + exception)
                            });
                    });
                    return user.uid;
                })
                .then(uid => {
                    return admin.firestore().collection('users')
                            .doc(uid)
                            .delete()
                            .then(() => {
                                console.log(`Document "${documentSnapshot.ref.path}" successfully deleted.`);
                                return true;
                            })
                            .catch(exception => {
                                console.log('Error!: ' + exception)
                            });
                })
                .catch(err => {
                    console.log('Error getting document', err);
                });
        } else {
            console.log('No es usuario googlecloud');
        }
    });
}
