exports.eliminarGastosDelGrupo = (functions, admin) => {
    return functions.firestore
        .document('grupos/{grupoID}')
        .onDelete((snap, context) => {
            const deletedGroup = context.params.grupoID;

            console.log('DOC DELETED-> ID: '+ deletedGroup);

            return admin.firestore().collection('gastos')
                .where('idGrupo', '==', deletedGroup)
                .get()
                .then(querySnapshot => {
                    querySnapshot.forEach(documentSnapshot => {
                        console.log(`Found document at ${documentSnapshot.ref.path}`);
                        admin.firestore()
                            .doc(documentSnapshot.ref.path)
                            .delete()
                            .then(() => {
                                console.log(`Document "${documentSnapshot.ref.path}" successfully deleted.`);
                                return true;
                            })
                            .catch(exception => {
                                console.log('Error!: ' + exception)
                            });
                    });
                    return deletedGroup;
                })
                .then(idGroup => {
                    admin.firestore().collection('users')
                        .where('grupos', 'array-contains', idGroup)
                        .get()
                        .then(querySnapshot => {
                            querySnapshot.forEach(documentSnapshot => {
                                console.log(`Found document at ${documentSnapshot.ref.path}`);
                                admin.firestore()
                                    .doc(documentSnapshot.ref.path)
                                    .update({
                                        grupos: admin.firestore.FieldValue.arrayRemove(idGroup)
                                    });
                            });
                            return true;
                        })
                        .catch(exception => {
                            console.log('Error!: ' + exception)
                        });
                    return true;
                })
                .catch(exception => {
                    console.log('Error!: ' + exception)
                });
        });
}
