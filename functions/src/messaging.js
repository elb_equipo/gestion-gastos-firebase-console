exports.enviarInvitacionGrupo = (functions, admin) => {
    return functions.https.onCall((data, context) => {
        console.log('Data del cliente: ', data);
        const payload = {
            data: {
                titulo: data.titulo,
                msg: data.msg,
                uid: data.uid,
                idGrupo: data.idGrupo
            }
        }
        try {
        return admin.messaging().sendToDevice(data.tokens, payload)
            .then((response) => {
                console.log('Successfully sent message:', response);
                if (response.failureCount > 0){
                    var tokens = data.tokens;
                    var results = response.results;
                    var tokensEliminar = new Array();

                    for (i = 0; i < tokens.length; i++) { 
                        if(results[i].error){
                            tokensEliminar.push(tokens[i]);
                        }
                    }
                    tokensEliminar.forEach(token => {
                        console.log("Token inválido eliminado. UID: ",data.uid," - Tokens: ",token);
                        admin.firestore().collection('users')
                            .doc(data.uid)
                            .update({
                                fcmToken: admin.firestore.FieldValue.arrayRemove(token)
                            })
                            .catch(exception => {
                                console.log('Error! Eliminar tokens inválidos: ' + exception)
                            });
                    })
                }
                if (response.successCount > 0){
                    console.log('Return TRUE');
                    return true;
                }else{
                    console.log('Return FALSE');
                    return false;
                }
            }); 
        }catch(err) {
            console.log('Error! Enviar notificación: ' + err.message);
            return 'no tokens';
        } 
    });
}
    