const functions = require('firebase-functions');

// Import files with functions
const messaging = require('./src/messaging');
const firestore = require('./src/firestore');
const authentication = require('./src/authentication');
// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp();

// Firestore
exports.eliminarGastosDelGrupo = firestore.eliminarGastosDelGrupo(functions, admin);

// Messaging
exports.enviarInvitacionGrupo = messaging.enviarInvitacionGrupo(functions, admin);

// Authentication
exports.eliminarUsuario = authentication.eliminarUsuario(functions, admin);